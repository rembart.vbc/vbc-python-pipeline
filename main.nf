#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.in = "raw_data.h5"
params.out = "results"
params.config = "${baseDir}/config.ini"


process parse {
    input:
      path 'input.h5.raw'
    output:
      path '*.h5'
    script:
    """
    #!/usr/local/bin/python3
    from configparser import ConfigParser
    from vbc.parser import Parser
    config = ConfigParser()
    config.read([
        '/code/config.ini', # in contianer default config
        '${params.config}' # optional user config
    ])
    parser = Parser(config)
    parser.run(input_file='input.h5.raw')
    """

}

process analyse {

    input:
      val input
    
    output:
      path '*.txt'
    script:
    """
    #!/usr/local/bin/python3
    from configparser import ConfigParser
    from vbc.processor import Processor
    config = ConfigParser()
    config.read([
        '/code/config.ini', # in contianer default config
        '${params.config}' # optional user config
    ])
    process = Processor(config)
    process.run(input_file='${input}')
    """
}

process render {

    input:
      path '*.txt'
    
    output:
      path 'result.png'

    publishDir 'results'

    script:
    """
    #!/usr/local/bin/python3
    from configparser import ConfigParser
    from vbc.render import Render
    config = ConfigParser()
    config.read([
        '/code/config.ini', # in contianer default config
        '${params.config}' # optional user config
    ])
    render = Render(config)
    render.run(input_directory='.')
    """
}

workflow {
  Channel.fromPath(params.in) | parse
  parse.out.flatten() | analyse
  analyse.out.collect() | render
}
