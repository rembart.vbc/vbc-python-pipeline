
# VBC Python Pipeline

This is a blueprint-demo pipeline to run self written python applications, in a standardised and reproducible way including HPC capabilities for faster executions.

The pipeline consists of three steps, the second step will be executed in parallel:

1. Parse
	Read a .h5 file, filter the consisting datasets according the the default/user config files.
	Split the .h5 file according to the top-level datasets.
2. Process
	For each reduced dataset, do some dummy transformation
3. Render
	Collect the processed files and render a dummy image

### Dataflow

```mermaid
graph LR
A[raw_data.h5] -- parse --> B((split_1.h5)) -- process --> F((res_data_1.txt)) -- render --> J[result.png] 
A[raw_data.h5] -- parse --> C((split_2.h5)) -- process --> G((res_data_2.txt)) -- render --> J[result.png] 
A[raw_data.h5] -- parse --> D((split_3.h5)) -- process --> H((res_data_3.txt)) -- render --> J[result.png] 
A[raw_data.h5] -- parse --> E((split_4.h5)) -- process --> I((res_data_4.txt)) -- render --> J[result.png] 
```


## Directories and Files
/code/vbc contains your scientific logic - put your python modules there
/code/test contains the test for your python logic

/Dockerfile contains the build instructions to run your python code independent from the underlying system. 
/gitlab-ci.yaml tests your code on each push, when you tag a commit it will also build ( if the testing was okay) and push the tag versioned image to the gitlab container registry.
/nextflow.config general nextflow configuration for singularity and the CBE cluster
/main.nf the business logic that connects the input/output files of parse/process/render and submits cluster jobs in parallel 


# Local Development
https://docs.python.org/3/library/venv.html

Example workflow:

    # clone a copy of the git reposiory
    git clone https://gitlab.com/rembart.vbc/vbc-python-pipeline.git
    cd vbc-python-pipeline.git
    # create a virtual environment to isolate python package from your local system
    python3 -m venv .venv
    # you have the activate the venv each time you start a new shell
    source .venv/bin/activate
    # install all python dependencies
    pip install -r code/requirements-dev.txt
    
    # checkout a new branche
    git checkout -b my-new-feature
    # change the code you want to change
    # edit, edit, edit
    # lets check in your changes to GIT
    git add -A
    git commit -m 'my new feature'
    git push 

# Code Testing
https://docs.python.org/3/library/unittest.html
https://coverage.readthedocs.io/en/6.4.1/
Simple test cases are located in the code/test directories, to execute the tests, use the coverage test wrapper:

    coverage run
    coverage report

# Build Pipeline
https://docs.gitlab.com/ee/ci/

A /gitlab-ci.yaml file is located in the root of the directory next to the /Dockerfile, it will execute the python tests on each push and build the container image for each tagged commit, and pushes it to the gitlab container repository.

# Build Local
https://docs.docker.com/engine/reference/builder/
To build a local image for testing:

    docker build .
    docker run -it [long-hash-from-build-command] python3 app.py parse -i ...
    
# Nextflow 
https://www.nextflow.io/docs/latest/

Nextflow is used as workflow engine, see the main.nf files in the root directory, in this case we execute the python libraries directly (within our container) from the nextflow script. alternatively you could start the app.py command and pass the parameters (use only one of these both approaches, to "dont repeat yourself")
The configuration nextflow.config is already adapted for default CBE cluster executions, feel free to adapt this to your beeds.


## Cluster (CBE)
https://docs.vbc.ac.at/books/scientific-computing/chapter/clip-batch-environment-%28cbe%29

    [user@clip-login-0 v0.5]$ ml nextflow/21.10.6
    [user@clip-login-0 v0.5]$ cp raw_data.h5 v0.5/  
    [user@clip-login-0 v0.5]$ nextflow run  https://gitlab.com/rembart.vbc/vbc-python-pipeline -r v0.5
    N E X T F L O W  ~  version 21.10.6
    Launching `rembart.vbc/vbc-python-pipeline` [soggy_gilbert] - revision: c9343fb4e6 [v0.5]
    [-  ] process > parse -
    executor >  slurm (6)
    [62/096eed] process > parse (1) [100%] 1 of 1 ✔
    [e3/beab29] process > analyse (1) [100%] 4 of 4 ✔
    [2e/1e7db4] process > render  [100%] 1 of 1 ✔
    **Completed at: 07-Jun-2022 20:27:55**
    **Duration**  **: 1m 36s**
    **CPU hours** **: (a few seconds)**
    **Succeeded** **: 6**
    
    [user@clip-login-0 v0.5]$ ls results/
    result.png
       

This starts six (1x parser, 4x processor, 1x render) cluster jobs, run the python software in an isolated (docker/singularity) and tested/versioned (v0.5) environment
