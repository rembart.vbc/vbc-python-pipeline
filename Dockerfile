FROM python:3.9.13-slim
ENV PYTHONPATH=${PYTHONPATH}:/code

COPY code /code
RUN /usr/local/bin/python3 -m pip install --upgrade pip &&\
    /usr/local/bin/python3 -m pip install -r /code/requirements-dev.txt

WORKDIR /code
