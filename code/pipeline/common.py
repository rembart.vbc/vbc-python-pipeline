"""
Some shared functions we can use to control the pipeline
"""
from configparser import ConfigParser


def read_configuration(user_config='', default_config='config.ini'):
    config = ConfigParser()
    config_files = config.read([default_config, user_config])
    if default_config not in config_files:
        print(default_config)
        raise Exception("The default configuration file was not found")
    if user_config and user_config not in config_files:
        raise Exception("The user config option was specified but the file was not found")
    return config
