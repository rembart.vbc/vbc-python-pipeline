from unittest import TestCase

from pipeline.common import read_configuration


class TestReadConfiguration(TestCase):

    def test_default_config_is_missing(self):
        with self.assertRaises(Exception) as ctx:
            read_configuration('', default_config='')
        self.assertTrue('The default configuration file was not found' in str(ctx.exception))

    def test_without_user_config(self):
        config = read_configuration(user_config='', default_config='tests/fixtures/config/default_config.ini')
        self.assertEqual(config['sectionA']['var1'], 'A-1')

    def test_with_user_config(self):
        config = read_configuration(
            user_config='tests/fixtures/config/user_config.ini',
            default_config='tests/fixtures/config/default_config.ini'
        )
        self.assertEqual(config['sectionA']['var1'], 'A-1-user')

    def test_user_config_is_missing(self):
        with self.assertRaises(Exception) as ctx:
            read_configuration(
                user_config='tests/fixtures/config/wrong_config.ini',
                default_config='tests/fixtures/config/default_config.ini'
            )
        self.assertTrue('The user config option was specified but the file was not found' in str(ctx.exception))
