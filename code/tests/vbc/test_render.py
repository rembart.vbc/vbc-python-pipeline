from os import listdir
from unittest import TestCase
from tempfile import TemporaryDirectory

from vbc.render import Render
from tests.fixtures.parser.case_a import case_a


class TestRender(TestCase):

    def test_config_is_load_on_init(self):
        config = {'render': {'destination': 'results'}}
        render = Render(config)
        self.assertEqual(render.config, config)

    def test_collect(self):
        config = {'render': {'destination': 'results'}}
        render = Render(config)
        all_results = render.collect('tests/fixtures/parser/')
        self.assertEqual(len(all_results), 40)

        for value in case_a['DS_A'] + case_a['DS_B'] + case_a['DS_C'] + case_a['DS_D']:
            self.assertIn(value, all_results)

    def test_picture(self):
        config = {'render': {'source': '', 'destination': 'kr'}}

        with TemporaryDirectory() as destination:
            config = {
                'render': {
                    'destination': destination,
                }
            }

            render = Render(config)
            render.run('tests/fixtures/parser/')

            # check for the resulting picture
            files = listdir(destination)
            self.assertTrue('result.png' in files)
