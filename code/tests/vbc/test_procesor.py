from unittest import TestCase
from tempfile import TemporaryDirectory
from os import listdir
from vbc.processor import Processor

from tests.fixtures.parser.case_a import case_a


class TestProcessor(TestCase):

    def test_config_is_load_on_init(self):
        config = {'processor': {'destination': 'results'}}
        processor = Processor(config)
        self.assertEqual(processor.config, config)

    def test_run_normal(self):

        file = 'tests/fixtures/parser/DS_A.h5'

        with TemporaryDirectory() as destination:

            config = {
                'processor': {
                    'tracer_reverse': 'true',
                    'destination': destination,
                    'hmm_multiplicator': '-1'
                }
            }
            processor = Processor(config)
            processor.run(file)

            # check for the resulting files
            files = listdir(destination)
            # Are all files created?
            self.assertEqual(len(files), 1)
            self.assertTrue('DS_A.txt' in files)

            # Check content of a single result file
            with open(destination + '/' + 'DS_A.txt', 'r') as test_a:
                content = test_a.readlines()
                content = [c.strip() for c in content]  # remove newlines from txt
                self.assertEqual(len(content), 10)  # ten Values in this dataset
                
                original = case_a['DS_A']
                original.reverse()
                original = [str(org * -1) for org in original]  # Transform original values according to running config
                self.assertEqual(
                    original,
                    content
                )
