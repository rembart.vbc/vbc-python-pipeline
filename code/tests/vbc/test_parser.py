from unittest import TestCase
from tempfile import TemporaryDirectory
from os import listdir
import h5py

from vbc.parser import Parser


class TestParser(TestCase):

    def test_config_is_load_on_init(self):
        config = {'parser': {'filter': 'all'}}
        parser = Parser(config)
        self.assertEqual(parser.config, config)

    def test_is_blacklisted(self):
        config = {'parser': {'blacklist': 'DS_A'}}
        parser = Parser(config)
        self.assertTrue(parser.is_blacklisted('DS_A'))
        self.assertFalse(parser.is_blacklisted('DS_B'))

    def test_is_blacklisted_list(self):
        config = {'parser': {'blacklist': 'DS_A,DS_B'}}
        parser = Parser(config)
        self.assertTrue(parser.is_blacklisted('DS_A'))
        self.assertTrue(parser.is_blacklisted('DS_B'))
        self.assertFalse(parser.is_blacklisted('DS_C'))

    def test_filter_values_none(self):
        test_data = [1, 2, 3, 0, -1, -2, -3]

        config = {'parser': {'limit': ''}}
        parser = Parser(config)
        self.assertEqual(parser.filter_values(test_data), test_data)

        config = {'parser': {'limit': 'none'}}
        parser = Parser(config)
        self.assertEqual(parser.filter_values(test_data), test_data)

        config = {'parser': {}}
        parser = Parser(config)
        self.assertEqual(parser.filter_values(test_data), test_data)

    def test_filter_values_pos(self):
        test_data = [1, 2, 3, 0, -1, -2, -3]
        config = {'parser': {'limit': '+'}}
        parser = Parser(config)
        self.assertEqual(parser.filter_values(test_data), [1, 2, 3, 0])

    def test_filter_values_neg(self):
        test_data = [1, 2, 3, 0, -1, -2, -3]
        config = {'parser': {'limit': '-'}}
        parser = Parser(config)
        self.assertEqual(parser.filter_values(test_data), [0, -1, -2, -3])

    def test_filter_values_invalid(self):
        test_data = [1, 2, 3, 0, -1, -2, -3]
        config = {'parser': {'limit': 'invalid'}}
        parser = Parser(config)
        with self.assertRaises(Exception) as ctx:
            self.assertEqual(parser.filter_values(test_data), [0, -1, -2, -3])

        self.assertTrue('Invalud configuration for parser->limit configuration' in str(ctx.exception))

    def test_run(self):
        file = 'tests/fixtures/parser/test.h5'

        with TemporaryDirectory() as destination:
            config = {'parser': {'limit': '+', 'blacklist': 'DS_B', 'destination': destination}}
            parser = Parser(config)
            parser.run(file)
            files = listdir(destination)
            # Are all files created?
            self.assertEqual(len(files), 3)
            self.assertTrue('DS_A.h5' in files)
            self.assertTrue('DS_C.h5' in files)
            self.assertTrue('DS_D.h5' in files)
            self.assertFalse('DS_B.h5' in files)
            # Check content of a single file
            with h5py.File(destination + '/' + 'DS_A.h5', 'r') as test_a:
                self.assertEqual(len(test_a.keys()), 1)

                self.assertTrue('DS_A' in test_a.keys())
                self.assertEqual(list(test_a['DS_A']), [1.1419739215455067, 1.6231058942113137])
