from unittest import TestCase

from vbc.tracer import Tracer


class TestTracer(TestCase):

    def test_config_is_load_on_init(self):
        config = {'processor': {'tracer_reverse': 'true'}}
        tracer = Tracer(config)
        self.assertEqual(tracer.config, config)

    def test_run_normal(self):
        config = {'processor': {'tracer_reverse': 'false'}}
        tracer = Tracer(config)
        self.assertEqual(tracer.run([1, 2, 3, 4]), [1, 2, 3, 4])

    def test_run_reverse(self):
        config = {'processor': {'tracer_reverse': 'True'}}
        tracer = Tracer(config)
        self.assertEqual(tracer.run([1, 2, 3, 4]), [4, 3, 2, 1])
