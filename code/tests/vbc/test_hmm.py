from unittest import TestCase

from vbc.hmm import Hmm


class TestHmm(TestCase):

    def test_config_is_load_on_init(self):
        config = {'processor': {'hmm_multiplicator': '-1'}}
        hmm = Hmm(config)
        self.assertEqual(hmm.config, config)

    def test_run_pos(self):
        config = {'processor': {'hmm_multiplicator': '2'}}
        hmm = Hmm(config)
        self.assertEqual(hmm.run([1, 2, 3, 4]), [2, 4, 6, 8])

    def test_run_neg(self):
        config = {'processor': {'hmm_multiplicator': '-1'}}
        hmm = Hmm(config)
        self.assertEqual(hmm.run([1, 2, 3, 4]), [-1, -2, -3, -4])

    def test_run_empty(self):
        config = {'processor': {'hmm_multiplicator': ''}}
        hmm = Hmm(config)
        self.assertEqual(hmm.run([1, 2, 3, 4]), [1, 2, 3, 4])

    def test_run_zero(self):
        config = {'processor': {'hmm_multiplicator': '0'}}
        hmm = Hmm(config)
        with self.assertRaises(Exception) as ctx:
            hmm.run([1, 2, 3, 4])
        self.assertTrue('The multiplicator in config processor->hmm_multiplicator can not be Zero' in str(ctx.exception))

    def test_run_invalid(self):
        config = {'processor': {'hmm_multiplicator': 'X'}}
        hmm = Hmm(config)
        with self.assertRaises(Exception) as ctx:
            hmm.run([1, 2, 3, 4])
        self.assertTrue('The multiplicator in config processor->hmm_multiplicator can not be casted to an int' in str(ctx.exception))