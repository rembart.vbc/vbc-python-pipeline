class Tracer:

    def __init__(self, config):
        self.config = config

    def run(self, datapoints):
        if 'tracer_reverse' in self.config['processor'] and self.config['processor']['tracer_reverse'].lower() == 'true':
            datapoints.reverse()
            return datapoints
        return datapoints
