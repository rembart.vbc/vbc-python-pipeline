import h5py
from vbc.tracer import Tracer
from vbc.hmm import Hmm


class Processor:
    """
    The entrypoint for your data processing
    It takes an already filted and splitted h5 file
    does the computations (eg.tracing, hmm,..) 
    writes the result to a file
    """
    def __init__(self, config):
        self.config = config

    def run(self, input_file):
        with h5py.File(input_file, 'r') as input:

            dataset = list(input)[0]
            datapoints = list(input[dataset])

            # Split up your processing application at this point to simplify architecture and testing
            tracer = Tracer(self.config)
            traces = tracer.run(datapoints)

            hmmer = Hmm(self.config)
            results = hmmer.run(traces)

            with open(self.config['processor']['destination'] + '/' + dataset + '.txt', "x") as f:
                for line in results:
                    f.write(str(line) + '\n')
