class Hmm:

    def __init__(self, config):
        self.config = config

    def run(self, datapoints):
        if 'hmm_multiplicator' in self.config['processor'] and len(self.config['processor']['hmm_multiplicator']) > 0:
            try:
                multiplicator = int(self.config['processor']['hmm_multiplicator'])
            except ValueError:
                raise Exception('The multiplicator in config processor->hmm_multiplicator can not be casted to an int')
            if not multiplicator:
                raise Exception('The multiplicator in config processor->hmm_multiplicator can not be Zero')
            return [datapoint * multiplicator for datapoint in datapoints]
        return datapoints
