import h5py


class Parser:
    def __init__(self, config):
        self.config = config

    def is_blacklisted(self, dataset):
        if self.config['parser']['blacklist'] and dataset in self.config['parser']['blacklist'].split(','):
            return True
        return False

    def filter_values(self, datapoints):
        if 'limit' not in self.config['parser'] or not self.config['parser']['limit'] or self.config['parser']['limit'] == 'none':
            return datapoints
        if self.config['parser']['limit'] == '+':
            return [value for value in datapoints if value >= 0]
        if self.config['parser']['limit'] == '-':
            return [value for value in datapoints if value <= 0]
        raise Exception('Invalud configuration for parser->limit configuration')

    def run(self, input_file):
        with h5py.File(input_file, 'r') as input:

            for dataset, datapoints in input.items():
                if self.is_blacklisted(dataset):
                    continue

                valid_datapoints = self.filter_values(datapoints)
                with h5py.File(self.config['parser']['destination'] + '/' + dataset + '.h5', 'w') as f:
                    f.create_dataset(dataset, data=valid_datapoints)
