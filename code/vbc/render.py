from os import listdir
import texttoimage


class Render:

    def __init__(self, config):
        self.config = config

    def collect(self, input_directory):
        files = listdir(input_directory)

        files = [input_directory + '/' + f for f in files if f.endswith('.txt')]
        result = []
        for file_path in files:
            with open(file_path, 'r') as f:
                content = f.readlines()
                result = result + [float(c.strip()) for c in content]

        return result

    def run(self, input_directory):
        all_results = self.collect(input_directory)
        result = sum(all_results)

        file_name = self.config['render']['destination'] + '/result.png'
        texttoimage.convert(str(result), image_file=file_name, font_size=100, color='red')
