"""
Entrypoints for pipeline commands
This is an example for a local entrypoint
If using nextflow, we will not need this at all
"""

from argparse import ArgumentParser
from pipeline.common import read_configuration

from vbc.parser import Parser
from vbc.processor import Processor
from vbc.render import Render

app = ArgumentParser(description='VBC python project')

# Global arguments
app.add_argument("-c", dest="config_file", required=False, help="path to config file", default='')

cmd = app.add_subparsers(dest='command', title='commands', description='valid commands', required=True)

# run = subparsers.add_parser('run', help='Run pipeline')
cmd_parser = cmd.add_parser('parse', help='Parse, filter and split raw data')
cmd_parser.add_argument("-i", dest="input_file", required=True, help="path to raw h5 data")

cmd_process = cmd.add_parser('process', help='Process parsed data')
cmd_process.add_argument("-i", dest="input_file", required=True, help="path to filtered h5 file")

cmd_render = cmd.add_parser('render', help='Render processed data')
cmd_render.add_argument("-i", dest="input_directory", required=True, help="path to results directory")

args = app.parse_args()

config = read_configuration(args.config_file)

if args.command == 'parse':
    parser = Parser(config)
    parser.run(input_file=args.input_file)

if args.command == 'process':
    process = Processor(config)
    process.run(input_file=args.input_file)

if args.command == 'render':
    render = Render(config)
    render.run(input_directory=args.input_directory)
